package domain

type Todo struct {
	ID        int
	Title     string
	Completed bool
	UserID    int
}
